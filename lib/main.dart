import 'package:activity_record/pages/diy_analysis.dart';
import 'package:activity_record/pages/diy_search.dart';
import 'package:activity_record/pages/home_page.dart';
import 'package:flutter/material.dart';

void main() => runApp(new MyApp()); //main函数是程序的主入口

class MyApp extends StatelessWidget {
  // final ThemeData kIOSTheme = new ThemeData(
  //   primarySwatch: Colors.blue,
  //   primaryColor: Color(0xff399DC8),
  //   primaryColorBrightness: Brightness.light,
  // );

  // final ThemeData kDefaultTheme = new ThemeData(
  //   primarySwatch: Colors.purple,
  //   accentColor: Colors.orangeAccent[400],
  // );

  @override
  Widget build(BuildContext context) {
    //返回一个material规范的app
    return new MaterialApp(
      title: 'Activity Record', //这个title是你打开任务管理器的时候显示的名字
      theme: new ThemeData(
        primaryColor: Color(0xff1e72cd),
      ),
      home: new HomePage(),
      // routes: <String, WidgetBuilder>{
      //   '/search': (BuildContext context) => new DiySearch()
      // },
    );
  }
}
