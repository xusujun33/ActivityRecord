import 'dart:typed_data';

import 'package:activity_record/model/diy_project.dart';
import 'package:activity_record/pages/diy_add_dialog.dart';
import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:carousel_slider/carousel_slider.dart';

class DiyItemInfo extends StatefulWidget {
  DiyItemInfo(
      {Key key,
      this.db,
      this.diyProject,
      this.diyProjects,
      this.unCheckedDiyProjects})
      : super(key: key);
  DiyProject diyProject;
  List<DiyProject> diyProjects;
  List<DiyProject> unCheckedDiyProjects;
  final db;

  @override
  State<StatefulWidget> createState() => new DiyItemInfoState();
}

class DiyItemInfoState extends State<DiyItemInfo> {
  //遍历数据库初始化_diyProjects数据
  _getDiyProjects() async {
    print('=========开始读取数据库全部数据=========');
    List result = await widget.db.getDiyProjects();
    widget.diyProjects.clear();
    if (result.length != 0) {
      setState(() {
        result
            .forEach((diy) => widget.diyProjects.add(DiyProject.fromMap(diy)));
      });
    }
  }

  //遍历数据库未结账的_diyProjects数据
  _getUnCheckedDiyProjects() async {
    print('=========开始读取数据库没有结账的数据=========');
    List result = await widget.db.getUnCheckedDiyProjects();
    widget.unCheckedDiyProjects.clear();
    if (result.length != 0) {
      setState(() {
        result.forEach(
            (diy) => widget.unCheckedDiyProjects.add(DiyProject.fromMap(diy)));
      });
    }
  }

  //根据拼接的图片字符串拆分成对应的uint8list数组
  List _getUint8List(String imagePath) {
    List p = [];
    if (imagePath.contains('&')) {
      List x = imagePath.split('&');
      x.forEach((value) {
        List y = value.split('-');
        List<int> z = [];
        y.forEach((string) {
          z.add(int.parse(string));
        });
        p.add(Uint8List.fromList(z));
      });
      return p;
    } else {
      List y = imagePath.split('-');
      List<int> z = [];
      y.forEach((string) {
        z.add(int.parse(string));
      });
      p.add(Uint8List.fromList(z));
      return p;
    }
  }

  //进入编辑页面
  _editDiyProject() async {
    final result = await Navigator.of(context).push(
      new MaterialPageRoute(
        builder: (context) {
          return new DiyAddDialog(
            db: widget.db,
            diyProject: widget.diyProject,
          );
        },
      ),
    );
    if (result != null) {
      setState(() {
        widget.diyProject = result;
        _getDiyProjects();
        _getUnCheckedDiyProjects();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(widget.diyProject.name),
          actions: <Widget>[
            new IconButton(
              icon: new Icon(Icons.border_color),
              onPressed: () {
                _editDiyProject();
              },
            )
          ],
        ),
        body: Container(
          child: new ListView(
            children: <Widget>[
              new CarouselSlider(
                items: _getUint8List(widget.diyProject.imagePath).map((value) {
                  return new Container(
                    margin: const EdgeInsets.all(5.0),
                    child: ClipRRect(
                      borderRadius: new BorderRadius.circular(5.0),
                      child: new Image.memory(
                        value,
                        width: MediaQuery.of(context).size.width,
                        fit: BoxFit.cover,
                      ),
                    ),
                  );
                }).toList(),
                autoPlay: true,
                viewportFraction: 0.9,
                aspectRatio: 2.0,
              ),
              //名称和联系人
              new ListTile(
                title: new Text(widget.diyProject.name,
                    style: new TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 22.0)),
                trailing: new Text(widget.diyProject.contact),
              ),
              //地点和日期
              new ListTile(
                title: new Text(widget.diyProject.place,
                    style: new TextStyle(fontWeight: FontWeight.w500)),
                trailing: new Text(widget.diyProject.date),
              ),
              //备注
              widget.diyProject.notes != ''
                  ? ListTile(
                      title: new Text('备注：${widget.diyProject.notes}',
                          style: new TextStyle(
                              fontWeight: FontWeight.w500,
                              fontStyle: FontStyle.italic)),
                    )
                  : new Container(
                      width: 0.0,
                      height: 0.0,
                    ),
              //活动地点时间联系人
              new Divider(
                indent: 16.0,
              ),
              //活动单价、份数、金额信息
              new Container(
                height: 80.0,
                padding: const EdgeInsets.only(left: 18.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        new Text('单价: ${widget.diyProject.singlePrcie}元'),
                        new Text('数量: ${widget.diyProject.nums}份'),
                        new Text('总价: ${widget.diyProject.totalAmount}元'),
                      ],
                    ),
                    new SizedBox(
                      width: 80.0,
                    ),
                    new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        new Text('物料: ${widget.diyProject.itemCost}元'),
                        new Text('人员: ${widget.diyProject.laborCost}元'),
                        new Text('利润: ${widget.diyProject.profit}元'),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
