import 'dart:io';

import 'package:activity_record/model/diy_project.dart';
import 'package:flutter/material.dart';

class DiySearch extends StatefulWidget {
  DiySearch({Key key, this.db}) : super(key: key);
  var db;
  @override
  State<StatefulWidget> createState() => new DiySearchState();
}

class DiySearchState extends State<DiySearch> {
  String _text = '查错了不关我的事';
  //定义一个查询结果的数组
  List<DiyProject> _searchResult = [];
  TextEditingController _searchTextController = new TextEditingController();

  //通过输入的字符串查询数据库
  _searchDiy(String value) async {
    var result = await widget.db.searchDiy(value);
    _searchResult.clear();
    setState(
      () {
        if (result != null) {
          result.forEach((value) {
            _searchResult.add(DiyProject.fromMap(value));
          });
          _text = '什么都没有查到';
        }
      },
    );
  }
  //每条查询结果的显示布局
  Widget _searchResultListTile(DiyProject diyProject) {
    return new Container(
      color: Colors.white,
      padding: const EdgeInsets.all(8.0),
      height: 100.0,
      child: new Row(
        children: <Widget>[
          new ClipRRect(
            borderRadius: BorderRadius.circular(4.0),
            child: new Image.file(
              File(diyProject.imagePath),
              fit: BoxFit.cover,
              width: 100.0,
            ),
          ),
          new Expanded(
            flex: 2,
            child: new Container(
              margin: const EdgeInsets.only(left: 10.0),
              padding: const EdgeInsets.all(4.0),
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text(
                    diyProject.name,
                    style: new TextStyle(fontWeight: FontWeight.bold),
                  ),
                  new Text(diyProject.date),
                  new Row(
                    children: <Widget>[
                      new Text('${diyProject.nums.toString()}元'),
                      new SizedBox(
                        width: 10.0,
                      ),
                      new Text('${diyProject.singlePrcie.toString()}份')
                    ],
                  ),
                ],
              ),
            ),
          ),
          new Container(
            padding: const EdgeInsets.all(4.0),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                new Text(diyProject.place),
                new Text(diyProject.contact),
              ],
            ),
          )
        ],
      ),
    );
  }
  //整个查询结果
  Widget _searchResultListView(List list) {
    return new Container(
      color: Colors.grey[200],
      child: new ListView.builder(
        itemCount: _searchResult.length * 2,
        itemBuilder: (BuildContext context, index) {
          if (index.isOdd) {
            return new Divider(
              height: 0.0,
            );
          }
          index = index ~/ 2;
          return _searchResultListTile(list[index]);
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        //当leading或action为null的时候，title控件将会占据他们的空间
        automaticallyImplyLeading: false,
        title: new Container(
          padding: const EdgeInsets.all(4.0),
          alignment: Alignment.center,
          height: 30.0,
          decoration: new BoxDecoration(
              color: Colors.grey[200],
              borderRadius: new BorderRadius.circular(12.0)),
          child: new Row(
            children: <Widget>[
              new Icon(
                Icons.search,
                color: Colors.grey[600],
              ),
              new Flexible(
                child: new TextField(
                  controller: _searchTextController,
                  autofocus: true,
                  decoration: InputDecoration.collapsed(
                      hintText: '活动名称、联系人',
                      hintStyle: Theme.of(context).textTheme.body1.copyWith(
                          color: Colors.grey[600],
                          fontStyle: FontStyle.italic)),
                  onChanged: (value) {
                    if (value != '') {
                      _searchDiy(value);
                    }
                    setState(() {
                      if (value == '') {
                        _searchResult.clear();
                      }
                    });
                  },
                ),
              ),
              new InkWell(
                child: Container(
                    decoration: new BoxDecoration(shape: BoxShape.circle,color: Colors.grey[400]),
                    child: new Icon(
                      Icons.clear,
                      color: Colors.white,
                      size: 19.0,
                    )),
                onTap: () {
                  setState(() {
                    _searchTextController.clear();
                    _searchResult.clear();
                  });
                },
              )
            ],
          ),
        ),
        actions: <Widget>[
          new FlatButton(
            padding: const EdgeInsets.all(0.0),
            child: new Text(
              '取消',
              style: Theme.of(context)
                  .textTheme
                  .body1
                  .copyWith(color: Colors.white),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          )
        ],
      ),
      body: _searchResult.length == 0
          ? new Center(
              child: new Text(_text, style: new TextStyle(fontSize: 18.0)),
            )
          : _searchResultListView(_searchResult),
    );
  }
}
